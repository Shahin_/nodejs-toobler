const { readSnapshot } = require("./snapshot");
class UserStore {
  constructor() {
    this.users = readSnapshot();
  }

  addUser(name) {
    this.users.push(name);
  }

  search(query) {
    return this.users.filter(item => item.indexOf(query) === 0);
  }

  deleteUser(argindex) {
    this.users = this.users.filter((item, index) => index !== argindex);
  }
}

module.exports = new UserStore();
