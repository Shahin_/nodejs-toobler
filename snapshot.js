const fs = require("fs");
const FILE_NAME = "./users.json";

function readSnapshot() {
  if (fs.existsSync(FILE_NAME)) {
    const snapshotJSON = fs.readFileSync(FILE_NAME);
    const parsedSnaphsot = JSON.parse(snapshotJSON);
    return parsedSnaphsot;
  } else {
    return [];
  }
}

function writeSnapshot(snapshotData) {
  const writePromise = new Promise((resolve, reject) => {
    const jsonSnapshot = JSON.stringify(snapshotData);
    fs.writeFile(FILE_NAME, jsonSnapshot, err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });

  return writePromise;
}

function readLineWithPromise(rl, question) {
  return new Promise((resolve, reject) => {
    rl.question(question, answer => {
      resolve(answer);
    });
  });
}

module.exports = {
  readSnapshot,
  writeSnapshot,
  readLineWithPromise
};
