const readline = require("readline");
const userStore = require("./userStore");
const { writeSnapshot, readLineWithPromise } = require("./snapshot");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

async function MainMenu() {
  console.clear();
  const answer = await readLineWithPromise(
    rl,
    "---- MAIN MENU ---- \n 1.Add User \n 2.Search User \n 3.Delete user \n 4.Print users \n 5.Exit\n "
  );
  switch (answer) {
    case "1": {
      addUser();
      break;
    }
    case "2": {
      searchUser();
      break;
    }
    case "3": {
      deleteUser();
      break;
    }
    case "4": {
      printAllUsers();
      break;
    }
    case "5": {
      rl.close();
      break;
    }
    default: {
      console.error("Invalid input");
      backtoMenu();
    }
  }
}

function backtoMenu() {
  setTimeout(MainMenu, 2000);
}

async function addUser() {
  console.clear();
  const user = { name: "", age: 0, about: "" };
  user.name = await readLineWithPromise(rl, "Enter your name\n");
  user.age = await readLineWithPromise(rl, "Enter your age\n");
  user.about = await readLineWithPromise(rl, "Enter your about\n");
  userStore.addUser(user);
  console.log("Saved user");
  backtoMenu();
}

function searchUser() {
  console.clear();
  rl.question("---- Search User ---- \n Enter your query \n", answer => {
    const results = userStore.search(answer);
    if (results.length === 0) {
      console.log("no users found!");
      backtoMenu();
      return;
    }
    results.forEach((item, index) => {
      console.log(`${index + 1}. ${item}\n`);
    });
    goBackMenu();
  });
}

function deleteUser() {
  console.clear();
  console.log("---- Delete User ----");
  userStore.users.forEach((item, index) => {
    console.log(`${index + 1}. ${item}\n`);
  });
  rl.question(" \n Select User \n", answer => {
    const index = parseInt(answer);
    if (index === NaN) {
      console.log("invalid number");
      backtoMenu();
      return;
    }
    userStore.deleteUser(index - 1);
    console.log("user deleted ");
    backtoMenu();
  });
}

function printAllUsers() {
  console.clear();
  console.log("---- Users ---- \n");
  userStore.users.forEach((item, index) => {
    console.log(`\n ${index + 1}. ${item.name}`);
    console.log(`age:  ${item.age}`);
    console.log(`about ${item.about}`);
    console.log(".............................\n");
  });
  goBackMenu();
}

function goBackMenu() {
  rl.question("Go back", answer => {
    MainMenu();
  });
}

MainMenu();

process.once("beforeExit", async () => {
  try {
    await writeSnapshot(userStore.users);
    console.log("Snapshot saved...");
  } catch (error) {
    console.log("Error while saving snapshot...", error);
  }
});
